require 'faker'

class Movie < ApplicationRecord
  def self.active_movies
    Movie.all.where(:is_active => true).order('id desc')
  end

  def self.faker
    Movie.create(
             :name          => Faker::Name.unique.name,
             :release_year  => ((1950 + rand(65)).to_s + "0606").to_date,
             :rating        => rand(5),
             :is_active     => true
    ) rescue nil
  end

  def self.add(params)
    Movie.create(
        :name         => params['name'],
        :release_year => params['release_year'] ,
        :rating       => params['rating'],
        :is_active    => params['is_active']
    ) rescue nil
  end

end
