class DataSubmissionController < ApplicationController
  def destroy_movie
    Movie.find(params['movie_id']).update(
                                      :is_active => false
    )
    redirect_back fallback_location: root_path
  end

  def update
    Movie.find(params['movie_id']).update(
        :name         => params['name'],
        :release_year => "#{params['year_name']}0606".to_date,
        :rating       => params['rating']
    )
    redirect_back fallback_location: root_path
  end

  def new
    Movie.add(
        params.merge(
            'is_active'    => true,
            'release_year' => "#{params['year_name']}0606".to_date
        )
    )
    redirect_back fallback_location: root_path
  end

end
