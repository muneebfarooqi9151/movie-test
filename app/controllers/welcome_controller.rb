class WelcomeController < ApplicationController
  def index
    @movies = Movie.active_movies
  end
end
