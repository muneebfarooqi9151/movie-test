Rails.application.routes.draw do
  root 'welcome#index'
  post 'movie' => 'data_submission#update'
  post 'movies' => 'data_submission#new'
  get 'destroy_movie' => 'data_submission#destroy_movie'
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
