class CreateMovies < ActiveRecord::Migration[5.2]
  def change
    create_table :movies do |t|
      t.string :name
      t.date :release_year
      t.integer :rating
      t.boolean :is_active

      t.timestamps
    end
    add_index :movies, :is_active
  end
end
